
const express = require('express');
const homeRoutes = require('../api/routes/home');
const addRoutes =require('../api/routes/add');
const authRoutes= require('../api/routes/auth');
const path = require('path');


//set up the cors config

//fix Handlebar
const Handlebars = require('handlebars');
const {allowInsecurePrototypeAccess} = require('@handlebars/allow-prototype-access');
const exphbs = require('express-handlebars');  //html templates rendering

//register Template Engine
const hbs=exphbs.create({
    defaultLayout: 'main',
    extname:'hbs',
    handlebars: allowInsecurePrototypeAccess(Handlebars)
});

export default ({app}) => {
    // app.use( (req, res,next)=>{
    //     res.header("Access-Control-Allow-Origin", "*");
    //     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    //     next()
    // })
    app.engine('hbs', hbs.engine);
    app.set('view engine', 'hbs');
    app.set('views',   'server/views'); // где будут хранится шаблоны
    app.use(express.urlencoded({extended: false})); // обработчик req.body  до роутов! это Middleware
    app.use('/add', addRoutes);
    app.use('/auth',authRoutes);
    app.use('/', homeRoutes);
    app.use(express.static('server/public'));
  // app.use(express.static(path.join(__dirname, 'server/public')));

}