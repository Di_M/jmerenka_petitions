
import config from '../config/index';
import mongoose from 'mongoose';
import dotenv from 'dotenv'

export default async () => {
    const mongo = await mongoose
        .connect(
          config.databaseURL,
            {
                useNewUrlParser: true,
                useUnifiedTopology: true
            }
        );

    return mongo.connection.db;
};