import jwt from 'jsonwebtoken';
import config from '../config/index';

const isAuth= (req, res, next)=>{
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)

    jwt.verify(token, config.securityToken, (err, user) => {
        console.log(err)
        if (err) return res.sendStatus(403)
        req.user = user
        next()
    })

}
// const isAuth = jwt({
//     secret: config.securityToken,
//     userProperty: 'token',
//     getToken: (request) => {
//         if (
//             request.headers.authorization &&
//             (
//                 request.headers.authorization.split(' ')[0] === 'Token' ||
//                 request.headers.authorization.split(' ')[0] === 'Bearer'
//             )
//         ) {
//             return request.headers.authorization.split(' ')[1];
//         }
//
//         return null;
//     }
// });
//
// export default isAuth;