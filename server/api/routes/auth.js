const {Router} = require('express');
import config from '../../config/index';
const bcrypt = require('bcryptjs');
const User = require('../../models/user');
const uuid = require('uuid/v4')
import jwt from 'jsonwebtoken';
const router = Router();

router.get('/login', async (req, res) => {
    res.render('auth/login', {
        title: 'Авторизация',
        isLogin: true
    })
});

router.post('/login', async(req, res)=>{
try{
    const {email, password}=req.body;
    const candidate = await User.findOne({email})

    if (candidate){
        const areSame=await bcrypt.compare(password, candidate.password)
        if (areSame) {
            // auth logic is here:
            console.log( 'User Signed Up')

            // JWT TOKEN add here
            const token =jwt.sign({
                    name:user.name,
                    email:user.email,
                    expiresIn:"10h"},
                config.securityToken)
            const refreshToken = uuid();

        } else {
            res.redirect('/auth/login#login')
        }
    } else {
        res.redirect('/auth/login#login')
    }

}catch(e){
    console.log(e)
}
});

router.get('/logout', async (req, res) => {
    //logic Logout
        res.redirect('/auth/login#login')
})

router.post('/register', async (req,res)=> {

    try {
        const {email, name, surname, sex, password} = req.body
        const candidate = await User.findOne({email})
        if (candidate){
            res.redirect('/auth/login#register')
        } else {
            const hashPassword=await bcrypt.hash(password, 10)
            const today = new Date();
            const user = new User({
                email, name, surname, sex, password:hashPassword, createdAt:today
            })
            await user.save()
            res.redirect('/auth/login#login')
        }
    } catch(e){
        console.log(e)
    }
});

module.exports = router;
