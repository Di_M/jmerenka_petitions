
const {Router} = require('express');
const Petition =require('../../models/petition');
//const isAuth=require('../../middleware/isAuth');
const router = Router();

router.get('/',  (req, res) => {
    res.render('add', {
        title: 'Добавить петицию',
        isAdd: true
    })
});

router.post('/', async (req, res) => {
    const today = new Date();
    console.log(req.body)
    res.status('200');

    const petition = new Petition({
        title: req.body.title,
        body: req.body.body,
        img: req.body.img,
        userId: '5e9726aa4273ccaf4aafe5af',   // temporary solution before fixing auth must be:  userId: req.user
        createdAt:today,
    })
    try {
        await petition.save()
        res.redirect('/')
    }catch (e){
        console.log(e)
    }
});


module.exports = router;