const {Router} = require('express')
const Petition = require('../../models/petition')
const router = Router()

router.get('/', async (req, res) => {
    const petitions = await Petition.find()
        .populate('userId', 'email name')
        .select('body title img createdAt')

    res.render('home', {
        title: 'Жмеренка Инфо',
        isHome: true,
        petitions
    })
})

// Set Separate Petition
router.get('/petitions/:id', async (req, res) => {
    try {
        const petition = await Petition.findById(req.params.id).populate('userId', 'name').select('body title img createdAt')
        console.log(petition)
        res.render('petition', {
            title: `Петиция ${petition.title}`,
            petition
        })
    } catch (e) {
        console.log(e);
    }

})


module.exports = router