const {Schema, model} = require('mongoose');

const voteSchema = new Schema({
    count: Number,
    petitionId: {
        type: Schema.Types.ObjectId,
        ref: 'Petition'
    },

});

module.exports = model('Vote', voteSchema);