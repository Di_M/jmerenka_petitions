const {Schema, model} = require('mongoose');

const userSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    name: String,
    surname:String,
    sex:String,
    password: {
        type: String,
        required: true
    },
    createdAt:Date,

});

module.exports = model('User', userSchema);