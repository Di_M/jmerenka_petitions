const {Schema, model} = require('mongoose');

const petitionSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    img: String,
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    createdAt:Date,

});
module.exports = model('Petition', petitionSchema);